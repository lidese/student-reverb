#include "math_utils.h"

/*

ftype my_round(ftype argIn) {
	ftype fl, ce, argOut;
	fl = floorf(argIn);
	ce = ceil(argIn);
	if (fabsf(argIn - fl) < fabsf(argIn - ce)) {
		argOut = fl;
	}
	else {
		argOut = ce;
	}
	return argOut;
}


bool is_nan(ftype *mas, int32_t mas_size) {
	bool result = false;
	for (int32_t n = 0; n < mas_size; n++) {
		if (isnan(mas[n])) {
			result = true;
		}
	}
	return result;
}


ftype get_mean_vec(ftype* Vector, int32_t VectorLength, int32_t IndexScale) {
	ftype mean_val = 0;
	for (int32_t i = 0; i < VectorLength; i++) {
		mean_val = (mean_val + Vector[i*IndexScale]);
	}
	mean_val = mean_val / VectorLength;
	return mean_val;
}


ftype get_hamming_window(int32_t size, ftype* w) {
	ftype sum = 0;
	for (int n = 0; n < size; n++) {
		w[n] = ftype(0.54 - 0.46 * cos((_M_PIx2 * n) / (size - 1)));
		sum = sum + w[n];
	}
	return sum;
}


ftype get_hann_window(int32_t size, ftype* w) {
	ftype sum = 0;
	for (int n = 0; n < size; n++) {
		w[n] = 0.5 * ftype(1 - cos((_M_PIx2 * n) / (size - 1)));
		sum = sum + w[n];
	}
	return sum;
}

*/