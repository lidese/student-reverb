#include "ReverbEffect.h"
#include <algorithm>
#include <iostream>


ReverbEffect::ReverbEffect()
{
	count_frame = 0;

	frame = new ftype[FRAME_BUFFER_SIZE];
	sig_in = new ftype[FRAME_BUFFER_SIZE];

	frame_l = new ftype[FRAME_BUFFER_SIZE];
	frame_r = new ftype[FRAME_BUFFER_SIZE];

	frame_orig = new ftype[FRAME_BUFFER_SIZE];

	d1_buf = new ftype[DELAY_BUFFER_SIZE];
	d2_buf = new ftype[DELAY_BUFFER_SIZE];
			
	echo_buf = createMatrixRow<ftype>(ECHO_FILTER_NUM, DELAY_BUFFER_SIZE);
	apf_buf = createMatrixRow<ftype>(ECHO_FILTER_NUM, DELAY_BUFFER_SIZE);

	apf_ptr = new int32_t[APF_NUM];
	echo_ptr = new int32_t[ECHO_FILTER_NUM];
			
	lp = new ftype[ECHO_FILTER_NUM];
		
	ge = new ftype[ECHO_FILTER_NUM];
	gm = new ftype[APF_NUM];

	fb = new ftype[APF_NUM];

	de = new int32_t[ECHO_FILTER_NUM];
	da = new int32_t[APF_NUM];

	gain_line_l = new ftype[FRAME_BUFFER_SIZE];
	gain_line_r = new ftype[FRAME_BUFFER_SIZE];

	xi = new ftype[FRAME_BUFFER_SIZE];
	yi = new ftype[FRAME_BUFFER_SIZE];
			
	a = 0.64; 
	b = 1 - a;
	fs = 44100;
	
	ga = 0.76;
	gr = 0.68;
	
	d1 = 11025; 
	d2 = 4410;
	
	//ge[0] = 0.5; ge[1] = 0.4; ge[2] = 0.4; ge[3] = 0.3; ge[4] = 0.2; ge[5] = 0.2;
	ge[0] = 0.68; ge[1] = 0.68; ge[2] = 0.68; ge[3] = 0.68; ge[4] = 0.68; ge[5] = 0.68;
	gm[0] = 0.3; gm[1] = 0.7; gm[2] = 0.7; gm[3] = 0.3;

	fb[0] = 0.84; fb[1] = 0.77; fb[2] = 0.71; fb[3] = 0.63;

	//de[0] = 3978; de[1] = 3722; de[2] = 2990; de[3] = 3947; de[4] = 1923; de[5] = 2597;
	//da[0] = 3233; da[1] = 3951; da[2] = 3771; da[3] = 2862;

	de[0] = 5600; de[1] = 5600; de[2] = 5600; de[3] = 5600; de[4] = 5600; de[5] = 5600;
	//da[0] = 6000; da[1] = 6000; da[2] = 6000; da[3] = 6000;
	da[0] = 6800; da[1] = 6800; da[2] = 6800; da[3] = 6800;
	
	for (int32_t n = 0; n < DELAY_BUFFER_SIZE; n++)	{
		d1_buf[n] = 0;
		d2_buf[n] = 0;
	}


	for (int32_t n = 0; n < FRAME_BUFFER_SIZE; n++) {
		xi[n] = n;
		yi[n] = 0;
	}


	for (int32_t k = 0; k < ECHO_FILTER_NUM; k++) {
		echo_ptr[k] = DELAY_BUFFER_SIZE - de[k];
		for (int32_t n = 0; n < DELAY_BUFFER_SIZE; n++) {
			echo_buf[k][n] = 0;
		}
	}
	
	for (int32_t k = 0; k < APF_NUM; k++) {
		lp[k] = 0;
		apf_ptr[k] = DELAY_BUFFER_SIZE - da[k];

		for (int32_t n = 0; n < DELAY_BUFFER_SIZE; n++) {
			apf_buf[k][n] = 0;
		}
	}

	d1_ptr = DELAY_BUFFER_SIZE - d1;
	d2_ptr = DELAY_BUFFER_SIZE - d2;

	alpha = 0.5;
	energy_control = 10e05;
	
}


ReverbEffect::~ReverbEffect()
{
	deleteMatrixRow<ftype>(apf_buf, ECHO_FILTER_NUM, DELAY_BUFFER_SIZE);
	deleteMatrixRow<ftype>(echo_buf, ECHO_FILTER_NUM, DELAY_BUFFER_SIZE);

	delete[] frame;

	delete[] frame_l;
	delete[] frame_r;

	delete[] d1_buf;
	delete[] d2_buf;

	delete[] apf_ptr;
	delete[] echo_ptr;

	delete[] lp;

	delete[] ge;
	delete[] gm;

	delete[] fb;

	delete[] de;
	delete[] da;

	delete[] gain_line_l;
	delete[] gain_line_r;

	delete[] xi;
	delete[] yi;

	delete[] frame_orig;
	delete[] sig_in;
}


void ReverbEffect::setApfDelays(ftype *da_ms, int32_t da_size)
{
	for (int32_t n = 0; n < da_size; n++)
	{
		da[n] = da_ms[n] * fs / 1000;
	}
}


void ReverbEffect::setEchoDelays(ftype *de_ms, int32_t de_size)
{
	for (int32_t n = 0; n < de_size; n++)
	{
		de[n] = de_ms[n] * fs / 1000;
	}
}


void ReverbEffect::setEchoGains(ftype *_ge, int32_t ge_size)
{
	for (int32_t n = 0; n < ge_size; n++) {
		ge[n] = _ge[n];
	}
}


void ReverbEffect::setMainDelays(ftype _d1, ftype _d2)
{
	d1 = _d1 * fs / 1000;
	d2 = _d2 * fs / 1000;
}


void ReverbEffect::setApfGain(ftype _ga)
{
	ga = _ga;
}


void ReverbEffect::setReverbGain(ftype _gr)
{
	gr = _gr;
}




void ReverbEffect::process(ftype* in_l, ftype* in_r, ftype *out_l, ftype *out_r, int32_t signal_size)
{

	int32_t ind;
	
	for (int32_t n = 0; n < signal_size; n++)
	{
		sig_in[n] = (in_l[n] + in_r[n]) / 2;
		frame_orig[n] = sig_in[n];
		sig_in[n] = sig_in[n] * gr;
	}
	
	for (int32_t n = 0; n < signal_size; n++)
	{
		frame[n] = d1_buf[d1_ptr];
		d1_buf[d1_ptr] = sig_in[n];

		d1_ptr = d1_ptr + 1;
		if (d1_ptr >= DELAY_BUFFER_SIZE) {
			d1_ptr = DELAY_BUFFER_SIZE - d1 - 1;
		}
				
		for (int32_t k = 0; k < ECHO_FILTER_NUM; k++)
		{
			ind = echo_ptr[k];
			frame[n] = frame[n] + ge[k] * echo_buf[k][ind];
			echo_buf[k][ind] = sig_in[n];

			echo_ptr[k] = echo_ptr[k] + 1;
			if (echo_ptr[k] >= DELAY_BUFFER_SIZE) {
				echo_ptr[k] = DELAY_BUFFER_SIZE - de[k] - 1;
			}

		}
		
		ftype apf_val = 0;

		for (int32_t k = 0; k < APF_NUM; k++)
		{
			ind = apf_ptr[k];
			lp[k] = b * apf_buf[k][ind] + a * lp[k];
			apf_buf[k][ind] = (1 - fb[k]) * frame[n] + fb[k] * lp[k];
			frame[n] = ga * lp[k] - fb[k] * frame[n];

			apf_ptr[k] = apf_ptr[k] + 1;
			if (apf_ptr[k] >= DELAY_BUFFER_SIZE)
			{
				apf_ptr[k] = DELAY_BUFFER_SIZE - da[k] - 1;
			}
		}
		
		frame_l[n] = frame[n];
		frame_r[n] = d2_buf[d2_ptr];
		d2_buf[d2_ptr] = frame[n];

		d2_ptr = d2_ptr + 1;
		if (d2_ptr >= DELAY_BUFFER_SIZE) {
			d2_ptr = DELAY_BUFFER_SIZE - d2 - 1;
		}

	}
	
	// compressor
	ftype current_energy = 0;
	for (int32_t n = 0; n < signal_size; n++)
	{
		current_energy = current_energy + sig_in[n] * sig_in[n];
	}
	current_energy = current_energy / signal_size;
	energy_control = current_energy * alpha + energy_control * (1 - alpha);

	level = 10 * log10(energy_control);

	new_gain_l = std::min<ftype>(-20 - 10 - level, 0);
	new_gain_l = pow(10, (new_gain_l / 20));
	new_gain_r = new_gain_l;

	ftype xk[2]; xk[0] = 0; xk[1] = signal_size;
	ftype yk[2]; yk[0] = last_gain_l; yk[1] = new_gain_l;

	MyFit(xk, yk, 2, &xi[1], gain_line_l, signal_size);

	for (int32_t n = 0; n < signal_size; n++)
	{
		frame_l[n] = frame_l[n] * gain_line_l[n];
		frame_r[n] = frame_r[n] * gain_line_l[n];
	}

	last_gain_l = new_gain_l;
	last_gain_r = new_gain_r;
	
	for (int32_t n = 0; n < signal_size; n++) {
		out_l[n] = frame_l[n] * gm[0] + frame_r[n] * gm[1];
		out_r[n] = frame_r[n] * gm[2] + frame_l[n] * gm[3];
	
		out_l[n] = out_l[n] + frame_orig[n];
		out_r[n] = out_r[n] + frame_orig[n];

		if ((out_l[n] > 0.99) || (out_l[n] < -0.99)) {
			std::cerr << "out_l is incorrect" << std::endl;
		}
	}

}




