#ifndef __DETECTOR_H_
#define __DETECTOR_H_

#include <cstdint>
#include <math.h>
#include "VcsTypes.h"
#include "utils.h"
#include "math_utils.h"

#include <mutex>


class ReverbEffect
{

public:

	ReverbEffect();
	~ReverbEffect();

	void setApfDelays(ftype* _da, int32_t da_size);
	void setEchoDelays(ftype* _de, int32_t de_size);
	void setEchoGains(ftype* _ge, int32_t ge_size);
	void setMainDelays(ftype _d1, ftype _d2);
	void setApfGain(ftype _ga);
	void setReverbGain(ftype _gr);

	void process(ftype* in_l, ftype* in_r, ftype *out_l, ftype *out_r, int32_t signal_size);
	
	ftype *frame;
	ftype *sig_in;

	ftype *frame_l;
	ftype *frame_r;

	ftype* frame_orig;
	
	ftype **echo_buf;
	ftype **apf_buf;

	int32_t *echo_ptr;
	int32_t *apf_ptr;
	
	int32_t frame_size;
	
	uint64_t count_frame;
	
	ftype a;
	ftype b;
	ftype fs;
	ftype gr;
	ftype ga;

	ftype *lp;
	ftype *ge;
	int32_t *de;
	int32_t *da;
	int32_t *dm;
	ftype *gm;
	ftype *fb;

	ftype *d1_buf;
	ftype *d2_buf;

	int32_t d1_ptr;
	int32_t d2_ptr;

	int32_t d1;
	int32_t d2;
		
	ftype alpha;
	ftype energy_control;
	ftype level;
	
	ftype new_gain_l;
	ftype new_gain_r;

	ftype last_gain_l;
	ftype last_gain_r;

	ftype *gain_line_l;
	ftype *gain_line_r;

	ftype *xi;
	ftype *yi;
	
	std::mutex *mutex;

};



















#endif
