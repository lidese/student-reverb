#include "utils.h"


void read_mas_f(std::fstream *file_stream, float **mas, int *size)
{
	(*file_stream).read((char*)(size), sizeof(itype));
	*mas = new float[*size];
	(*file_stream).read((char*)(*mas), (*size) * sizeof(float));
}


void read_mas_i(std::fstream *file_stream, int **mas, int *size)
{
	(*file_stream).read((char*)(size), sizeof(int));
	*mas = new int[*size];
	(*file_stream).read((char*)(*mas), (*size) * sizeof(int));
}


