#ifndef __UTILS_H_
#define __UTILS_H_

#include "VcsTypes.h"
#include <stdlib.h>
#include <stdio.h>
#include <fstream>


void read_mas_f(std::fstream *file_stream, float **mas, int *size);
void read_mas_i(std::fstream *file_stream, int **mas, int *size);


template <typename element_type>
element_type** createMatrixRow(int n_row, int n_col)
{
	element_type** matrix = new element_type*[n_row];
	for (int i = 0; i < n_row; i++)
	{
		matrix[i] = new element_type[n_col];
	}
	return matrix;
}

template <typename element_type>
void deleteMatrixRow(element_type** matrix, int n_row, int n_col)
{
	for (uint32_t i = 0; i < n_row; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;
}




#endif