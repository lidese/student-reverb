#ifndef __MATHUTILS_H_
#define	__MATHUTILS_H_

#include "VcsTypes.h"


template <typename T1>
int32_t MyFit(T1 *X1, T1 *Y1, int32_t n1, T1 *X2, T1 *Res, int32_t n2) {

	int32_t Ind1 = 0;
	int32_t Ind2;

	for (Ind2 = 0; Ind2 < n2; Ind2++) {

		while (Ind1 < n1 && X2[Ind2] > X1[Ind1])
			Ind1++;

		if (Ind1 > n1 - 1)
			break;

		if (Ind1 > 0)
			Res[Ind2] = Y1[Ind1 - 1] + (Y1[Ind1] - Y1[Ind1 - 1])*(X2[Ind2] - X1[Ind1 - 1]) / (X1[Ind1] - X1[Ind1 - 1]);
		else
			Res[Ind2] = Y1[0];
	}

	if (Ind1 > n1 - 1)
		//for (Ind2=Ind2; Ind2 < n2-1; Ind2++)
		for (Ind2 = Ind2; Ind2 < n2; Ind2++)
			Res[Ind2] = Y1[n1 - 1];

	return 0;
}


#endif //MATHUTILS_H
