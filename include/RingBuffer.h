#ifndef __RINGBUFFER_H_
#define __RINGBUFFER_H_


#include "VcsTypes.h"
//#include "MatDataIO.h"
#include <string.h>

#define TO_BEGIN 0
#define TO_END 1

#define ELEMENTS 0
#define COLUMNS 1


#define FILE_BUFFER_SIZE 44800


template <class T> class RingBuffer{
private:
public:
	T *Buffer;
	T **pBuffer;
	itype BufferSize;
	itype ColumnSize;
	itype StartIndex;
	itype EndIndex;
	itype FlagDir; // 0 -- add elements to begin; 1 -- add elements to end; 
	itype CurrentSize;
	itype FlagMultiDim;
	bool FlagOverFlow;

	//RingBuffer(int _BufferSize);
	//RingBuffer(int _BufferSize, int dim);

	// Creating of RingBuffer which contains columns of elements 
	RingBuffer(itype _BufferSize, itype dim){
		this->BufferSize = _BufferSize;
		this->ColumnSize = dim;
		this->pBuffer = new T*[BufferSize];

		for (itype i = 0; i < BufferSize; i++){
			pBuffer[i] = new T[dim];
		}

		this->Buffer = NULL;
		this->StartIndex = 0;
		this->EndIndex = 0;
		this->CurrentSize = 0;
		this->FlagOverFlow = false;
		this->FlagMultiDim = COLUMNS;
	}

	// Creating of RingBuffer which contains only elements 
	RingBuffer(itype _BufferSize)
	{
		this->BufferSize = _BufferSize;
		this->Buffer = new T[BufferSize];
		this->pBuffer = NULL;
		this->ColumnSize = 1;
		this->StartIndex = 0;
		this->EndIndex = 0;
		this->CurrentSize = 0;
		this->FlagOverFlow = false;
		this->FlagMultiDim = ELEMENTS;
	}

	// Creating of RingBuffer which contains data (elements) from file 
	RingBuffer(char *FileName, itype FlagDir){

		MatDataIO<T> Data;
		Data.MatRead(FileName);
		//this->BufferSize = Data.cols + 1;
		this->BufferSize = FILE_BUFFER_SIZE;
		this->ColumnSize = Data.rows;
		this->StartIndex = 0;
		this->EndIndex = 0;
		this->CurrentSize = 0;
		this->FlagOverFlow = false;

		// RingBuffer *RB=new RingBuffer(Data.cols+1,Data.rows);

		//if (BufferSize > 1000000)
		//{
		//	int a = 0;

		//}


		if (Data.rows > 1){

			this->Buffer = NULL;
			this->FlagMultiDim = COLUMNS;

			this->pBuffer = new T*[BufferSize];

			for (itype i = 0; i < BufferSize; i++){
				pBuffer[i] = new T[Data.rows];
			}

			for (itype i = 0; i < Data.cols; i++)
				Write(Data.data + i * Data.rows, FlagDir);


		}
		else{
			this->pBuffer = NULL;
			this->FlagMultiDim = ELEMENTS;




			this->Buffer = new T[BufferSize];
			Write(Data.data, Data.cols, FlagDir);
		}
	}

	// Creating of RingBuffer which contains data (columns) from file 
	RingBuffer(char *FileName, itype FlagDir, itype Col_size){

		MatDataIO<T> Data;
		Data.MatRead(FileName);
		//this->BufferSize = Data.cols + 1;
		this->BufferSize = Col_size;
		this->ColumnSize = Data.rows;
		this->StartIndex = 0;
		this->EndIndex = 0;
		this->CurrentSize = 0;
		this->FlagOverFlow = false;

		// RingBuffer *RB=new RingBuffer(Data.cols+1,Data.rows);
		if (Data.rows > 1){

			this->Buffer = NULL;
			this->FlagMultiDim = COLUMNS;

			this->pBuffer = new T*[BufferSize];

			for (itype i = 0; i < BufferSize; i++){
				pBuffer[i] = new T[Data.rows];
			}

			for (itype i = 0; i < Data.cols; i++)
				Write(Data.data + i * Data.rows, FlagDir);


		}
		else{
			this->pBuffer = NULL;
			this->FlagMultiDim = ELEMENTS;

			this->Buffer = new T[BufferSize];
			Write(Data.data, Data.cols, FlagDir);
		}
	}

	~RingBuffer(){
		if (Buffer != NULL)
			delete[] Buffer;
		else{
			for (itype i = 0; i < BufferSize; i++){
				if (pBuffer[i] != NULL)
					delete[] pBuffer[i];
			}
			if (pBuffer != NULL)
				delete[] pBuffer;
		}

	}


	void CopyFast1D(T* SubBuffer, itype SubBufferSize)
	{
		
		if ((StartIndex - SubBufferSize) >= 0)
		{
			memcpy(&SubBuffer[0], &Buffer[StartIndex], sizeof(T)*SubBufferSize);
		}
		else
		{
			Copy(0, SubBufferSize - 1, SubBuffer);
		}

		return;
	}



	// write data from SubBuffer size of SubBufferSize elements
	itype Write(T* SubBuffer, itype SubBufferSize, itype FlagDir){

		if (FlagDir == TO_BEGIN)
		{
			if ((StartIndex - SubBufferSize) >= 0){
				StartIndex = StartIndex - SubBufferSize;
				memcpy(&Buffer[StartIndex], &SubBuffer[0], sizeof(T)*SubBufferSize);
			}
			else{
				StartIndex = BufferSize + StartIndex - SubBufferSize;
				memcpy(&Buffer[StartIndex], &SubBuffer[0], sizeof(T)*(BufferSize - StartIndex));
				memcpy(&Buffer[0], &SubBuffer[(BufferSize - StartIndex)], sizeof(T)*(SubBufferSize - (BufferSize - StartIndex)));
			}
			//CurrentSize = BufferSize - StartIndex + EndIndex;
			CurrentSize = CurrentSize + SubBufferSize;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}

		if (FlagDir == TO_END)
		{
			if ((EndIndex + SubBufferSize) < BufferSize){
				memcpy(&Buffer[EndIndex], &SubBuffer[0], sizeof(T)*SubBufferSize);
				EndIndex = EndIndex + SubBufferSize;
			}
			else{
				memcpy(&Buffer[EndIndex], &SubBuffer[0], sizeof(T) * (BufferSize - EndIndex));
				memcpy(&Buffer[0], &SubBuffer[(BufferSize - EndIndex)], sizeof(T)*(SubBufferSize - (BufferSize - EndIndex)));
				EndIndex = SubBufferSize - (BufferSize - EndIndex);
			}
			//CurrentSize = BufferSize - StartIndex + EndIndex;
			CurrentSize = CurrentSize + SubBufferSize;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}

		if (FlagMultiDim == COLUMNS)
			std::cerr << "Error in Write method (RingBuffer.h)" << std::endl;

		return -1;

	}



	// write data (one column) from SubBuffer size of ColumnSize 
	itype Write(T* SubBuffer, itype FlagDir){
		if (FlagDir == TO_BEGIN)
		{
			if ((StartIndex - 1) >= 0){
				StartIndex = StartIndex - 1;
			}
			else{
				StartIndex = BufferSize + StartIndex - 1;
			}
			memcpy(pBuffer[StartIndex], &SubBuffer[0], sizeof(T)*ColumnSize);
			CurrentSize++;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}
		if (FlagDir == TO_END)
		{
			memcpy(pBuffer[EndIndex], &SubBuffer[0], sizeof(T)*ColumnSize);
			if ((EndIndex + 1) < BufferSize){
				EndIndex = EndIndex + 1;
			}
			else{
				EndIndex = 1 - (BufferSize - EndIndex);
			}
			CurrentSize++;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}
		return -1;
	}


	itype WriteNull(itype FlagDir){
		if (FlagDir == TO_BEGIN)
		{
			if ((StartIndex - 1) >= 0){
				StartIndex = StartIndex - 1;
			}
			else{
				StartIndex = BufferSize + StartIndex - 1;
			}
			//memcpy(pBuffer[StartIndex], &SubBuffer[0], sizeof(T)*ColumnSize);		
			pBuffer[StartIndex] = NULL;
			CurrentSize++;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}
		if (FlagDir == TO_END)
		{
			//memcpy(pBuffer[EndIndex], &SubBuffer[0], sizeof(T)*ColumnSize);
			pBuffer[EndIndex] = NULL;
			if ((EndIndex + 1) < BufferSize){
				EndIndex = EndIndex + 1;
			}
			else{
				EndIndex = 1 - (BufferSize - EndIndex);
			}
			CurrentSize++;
			if (CurrentSize > BufferSize)
				FlagOverFlow = true;
			return CurrentSize;
		}
		return -1;
	}



	// Copy elements (from n1 to n2) to destination "dest" 
	void Copy(itype n1, itype n2, T* dest){
		for (itype i = 0; i < n2 - n1 + 1; i++){
			dest[i] = GetElement(n1 + i);
		}
	}


	// Copy elements (or columns) to destination "dest" from n1 to n2 
	void Copy(itype n1, itype n2, RingBuffer<T> *dest, itype FlagDir){
		if (FlagMultiDim == ELEMENTS){
			for (itype i = 0; i < n2 - n1 + 1; i++){
				dest->Write(GetPointer(n1 + i), 1, FlagDir);
			}
		}
		if (FlagMultiDim == COLUMNS){
			for (itype i = 0; i < n2 - n1 + 1; i++){
				dest->Write(GetPointer(n1 + i, 0), FlagDir);
			}

		}
	}

	// Copy elements (or columns) to destination "dest" from n1 to n2 
	void Copy_safe(itype n1, itype n2, RingBuffer<T> *dest, itype FlagDir){
		if (n2 < n1)
			return;

		if (FlagMultiDim == ELEMENTS){
			for (itype i = 0; i < n2 - n1 + 1; i++){
				dest->Write(GetPointer(n1 + i), 1, FlagDir);
			}
		}
		if (FlagMultiDim == COLUMNS){
			for (itype i = 0; i < n2 - n1 + 1; i++){
				dest->Write(GetPointer(n1 + i, 0), FlagDir);
			}

		}
	}


	// Copy elements (from n1 to n2) to destination "dest" (fast version)
	// Only for ELEMENTS
	void CopyFast(itype n1, itype n2, RingBuffer<T> *dest, itype FlagDir){

		itype ind_n1 = GetIndex(n1);
		itype ind_n2 = GetIndex(n2);
		itype size_1;
		itype size_2;

		//if (n1 >= 0 && n2 >= 0){
		if (ind_n1 <= ind_n2){
			dest->Write(GetPointer(n1), n2 - n1 + 1, TO_END);
		}
		else{
			size_1 = BufferSize - ind_n1;
			size_2 = ind_n2 + 1;
			dest->Write(GetPointer(n1), size_1, TO_END);
			dest->Write(&Buffer[0], size_2, TO_END);
		}
		//}
	}


	// Copy elements (from n1 to n2) to destination "dest" (fast version)
	// Only for ELEMENTS
	void CopyFast_safe(itype n1, itype n2, RingBuffer<T> *dest, itype FlagDir){

		if (n2 < n1)
			return;

		itype ind_n1 = GetIndex(n1);
		itype ind_n2 = GetIndex(n2);
		itype size_1;
		itype size_2;

		//if (n1 >= 0 && n2 >= 0){
		if (ind_n1 <= ind_n2){
			dest->Write(GetPointer(n1), n2 - n1 + 1, TO_END);
		}
		else{
			size_1 = BufferSize - ind_n1;
			size_2 = ind_n2 + 1;
			dest->Write(GetPointer(n1), size_1, TO_END);
			dest->Write(&Buffer[0], size_2, TO_END);
		}
		//}
	}

	// Delete NumElements elements (columns) from buffer
	itype Clear(itype NumElements, itype FlagDir){


		//if ((CurrentSize - NumElements) < 0)
		//{
		//	int a = 0;

		//}



		if (FlagMultiDim == ELEMENTS){

			if (FlagDir == TO_BEGIN){
				if (StartIndex + NumElements < BufferSize)
					StartIndex = StartIndex + NumElements;
				else
					StartIndex = (StartIndex + NumElements) - BufferSize;
			}

			if (FlagDir == TO_END){
				if (EndIndex - NumElements >= 0)
					EndIndex = EndIndex - NumElements;
				else
					EndIndex = (EndIndex - NumElements) + BufferSize;
			}
			CurrentSize = CurrentSize - NumElements;
			return CurrentSize;
		}

		if (FlagMultiDim == COLUMNS){
			if (FlagDir == TO_BEGIN){
				if (StartIndex + NumElements < BufferSize)
					StartIndex = StartIndex + NumElements;
				else
					StartIndex = (StartIndex + NumElements) - BufferSize;
			}

			if (FlagDir == TO_END){
				if (EndIndex - NumElements >= 0)
					EndIndex = EndIndex - NumElements;
				else
					EndIndex = (EndIndex - NumElements) + BufferSize;
			}
			CurrentSize = CurrentSize - NumElements;
			return CurrentSize;
		}

		return -1;
	}

	// Getting index of element (column) 
	itype GetIndex(itype Index){
		if (FlagMultiDim == ELEMENTS){
			if (StartIndex + Index < BufferSize)
				return StartIndex + Index;
			else
				return (StartIndex + Index) - BufferSize;
		}
		if (FlagMultiDim == COLUMNS){
			if (StartIndex + Index < BufferSize)
				return StartIndex + Index;
			else
				return (StartIndex + Index) - BufferSize;

		}
		return -1;
	}


	// Getting value of element
	T GetElement(itype Index){


		//if (CurrentSize == 0)
		//{
		//	int a = 0;

		//}



		if (FlagMultiDim == ELEMENTS){
			/*		return Buffer[(StartIndex + Index + BufferSize)%BufferSize];*/
			if (StartIndex + Index < BufferSize)
				return Buffer[StartIndex + Index];
			else
				return Buffer[(StartIndex + Index) - BufferSize];
		}
		if (FlagMultiDim == COLUMNS){
			if (StartIndex + Index < BufferSize)
				return *pBuffer[StartIndex + Index];
			else
				return *pBuffer[(StartIndex + Index) - BufferSize];
		}

		return NULL;
	}



	void SetElement(itype Index, T Val){
		if (StartIndex + Index < BufferSize)
			Buffer[StartIndex + Index] = Val;
		else
			Buffer[(StartIndex + Index) - BufferSize] = Val;
	}


	// Getting value of element by column and row indices
	// Only for columns
	ftype GetElement(itype col_index, itype row_index){
		if (StartIndex + col_index < BufferSize)
			return pBuffer[StartIndex + col_index][row_index];
		else
			return pBuffer[(StartIndex + col_index) - BufferSize][row_index];
	}



	void SetElement(itype col_index, itype row_index, T Val){
		if (StartIndex + col_index < BufferSize)
			pBuffer[StartIndex + col_index][row_index] = Val;
		else
			pBuffer[(StartIndex + col_index) - BufferSize][row_index] = Val;
	}


	// Getting pointer of element by column and row indices
	// Only for COLUMNS
	T* GetPointer(itype col_index, itype row_index)
	{
		T *p_col = (T*)(&pBuffer[GetIndex(col_index)][row_index]);
		return p_col;
	}


	// Getting pointer of element by index
	// Only for ELEMENTS
	T* GetPointer(itype index)
	{
		T *p_elem = (T*)(&Buffer[GetIndex(index)]);
		return p_elem;
	}



	itype GetBufferSize()
	{
		return BufferSize;
	}


	itype GetCurrentSize()
	{
		return CurrentSize;
	}


	// Save data from buffer to file
	itype SaveToFile(char *FileName){

		std::fstream File;

		File.open(FileName, std::ios::binary | std::ios::out);
		if (!File.is_open()){
			std::cerr << FileName << " file is not opened!" << std::endl;
			return 1;
		}

		File.write((char*)&ColumnSize, sizeof(itype));
		File.write((char*)&CurrentSize, sizeof(itype));

		//itype size = ColumnSize * CurrentSize;

		if (FlagMultiDim == COLUMNS){
			for (itype i = 0; i < CurrentSize; i++)
				File.write((char*)GetPointer(i, 0), ColumnSize * sizeof(T));
		}
		else{
			for (itype i = 0; i < CurrentSize; i++)
				File.write((char*)GetPointer(i), 1 * sizeof(T));
		}


		File.close();
		return 0;
	}


};


#endif /* RINGBUFFER_H_ */
