#ifndef __VCSTYPES_H_
#define __VCSTYPES_H_

#include <inttypes.h>


#define PITCH_SEPARATION_LEVEL 25.0 
//#define AUTO_MIC_LEVEL

#define VOICER_BUFFER_SIZE 8000
#define	RB_BUFFER_SIZE 20000
#define RB_CONTOUR_SIZE 200000

#define RATING_DATA_FROM_KBF 0
#define RATING_DATA_FROM_PROCESSING 1

//#define PRINT_DEBUG_MESSAGES
//#define PRINT_ERROR_MESSAGES

#define MOBILE_VERSION	
//#define STAT_SOCKET
//#define KBF_FORMAT_DESKTOP
#define KBF_FORMAT_NEW

typedef float ftype; // floating point type for ringbuffer
typedef int32_t itype; // integer type for ringbuffer

#define EFFECT_ALL_OFF	0
#define EFFECT_PROCESS	1
#define EFFECT_OKTAVE	2
#define EFFECT_VOICE    4
#define EFFECT_COMPRESS	8
#define EFFECT_MIXER	16
#define EFFECT_REVERB	32
#define EFFECT_CHORUS	64
#define EFFECT_PONG		128
#define EFFECT_256		256
#define EFFECT_512		512
#define EFFECT_TEST		18446744073709551615

#define ID_DATA_CONTOUR				0
#define ID_DATA_EFFECT_PROCESS		1
#define ID_DATA_EFFECT_OKTAVE		2
#define ID_DATA_EFFECT_VOICE		3
#define ID_DATA_EFFECT_COMPRESS		4
#define ID_DATA_EFFECT_MIXER		5
#define ID_DATA_EFFECT_REVERB		6
#define ID_DATA_EFFECT_CHORUS		7
#define ID_DATA_EFFECT_PONG			8
#define ID_DATA_EFFECT_TEST			100

extern int flag_stop;

enum GRANULARITY
{
	g224,
	g256
};


#define MIDI_A4 69
#define FREQ_A4 440


#define FRAME_BUFFER_SIZE 8000
#define DELAY_BUFFER_SIZE 24000

#define ECHO_FILTER_NUM 6
#define APF_NUM 4




#endif
